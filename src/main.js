import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import Fragment from 'vue-fragment';
import Intro from './components/Intro.vue';
import Appointment from './components/Appointment.vue';
import CarDoctorRepair from './components/CarDoctorRepair.vue';
import CarDoctorUpgrades from './components/CarDoctorUpgrades.vue';
import CarDoctorFleetMaintenance from './components/CarDoctorFleetMaintenance.vue';
import AirManipulatorRepair from './components/AirManipulatorRepair.vue';
import AirManipulatorInstallatiom from './components/AirManipulatorInstallation.vue';
import AirManipulatorMaintenancePlan from './components/AirManipulatorMaintenancePlan.vue';
//import AboutUs from './components/AboutUs.vue';

Vue.use(VueRouter);
Vue.use(Fragment.Plugin);
Vue.config.productionTip = false;

const routes = [
  { path: '/', component: Intro },
  { name: 'Car Doctor/Repair', path: '/car-doctor/repair', component: CarDoctorRepair },
  { name: 'Car Doctor/Upgrades', path: '/car-doctor/upgrades', component: CarDoctorUpgrades },
  { name: 'Car Doctor/Fleet Maintenance', path: '/car-doctor/fleet-maintenace', component: CarDoctorFleetMaintenance },
  { name: 'Air Manipulator/Repair', path: '/air-manipulator/repair', component: AirManipulatorRepair },
  { name: 'Air Manipulator/Installation', path: '/air-manipulator/installation', component: AirManipulatorInstallatiom },
  { name: 'Air Manipulator/Maintenance Plans', path: '/air-manipulator/maintenance-plans', component: AirManipulatorMaintenancePlan },
  //{ name: 'About Us', path: '/about-us', component: AboutUs },
  { path: '/schedule-appointment', component: Appointment }
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  render: h => h(App),
  router
}).$mount('#app');


