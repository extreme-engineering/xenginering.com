# xenginering.com

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


gcloud init
gcloud auth configure-docker
gcloud config configurations activate extremeengineering
docker build -t gcr.io/extremeengineering/extremeengineering:1.0.0 .
docker push gcr.io/extremeengineering/extremeengineering:1.0.0
// create static ip
gcloud compute addresses create extremeengineering --global
// create kubernetes cluster
gcloud container clusters get-credentials primary --zone us-central1-a
kubectl run xenginering --image=gcr.io/extremeengineering/extremeengineering:1.0.0 --port 80
kubectl expose deployment xenginering --type=LoadBalancer --port 80 --target-port 80
kubectl get service